//
//  NetworkManager1.swift
//  2023-01-20-[StephenByron]-NYCSchools
//
//  Created by Stephen on 01/20/23.
//

import Foundation

struct Constants {
        static let schoolList = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        static let satInfo = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
    }

